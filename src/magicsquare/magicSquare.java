package magicsquare;
import java.util.*;
public class magicSquare {
    public static boolean isMagicSquare(int[][] arr,int n){
        int diagnolSum1 = 0;
        int diagnolSum2 = 0;
        for (int i=0;i<n;i++) {

            diagnolSum1 += arr[i][i];
            diagnolSum2 += arr[i][n - i - 1];

        }
        if (diagnolSum1 != diagnolSum2) {
            return false;
        }
        for (int i=0;i<n;i++){
            int rowSum=0;
            int columnSum=0;
            for(int j=0;j<n;j++){

                rowSum+=arr[i][j];
                columnSum+=arr[j][i];

            }
            if (rowSum!=columnSum){
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[][] arr=new int[n][n];
        for (int i=0;i<n;i++){
            for (int j=0;j<n;j++){
                arr[i][j]=sc.nextInt();
            }
        }
        if (isMagicSquare(arr,n)){
            System.out.println("yes");
        }
        else System.out.println("no");

    }
}
